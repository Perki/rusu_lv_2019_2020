import sys

ocjena = input ("Unesite ocjenu: ")

try:
    ocjena = float(ocjena)
except ValueError:
    print("Nema broja!")
    sys.exit()

print(type(ocjena))
ocjena_str=""
if ocjena >= 0.0 and ocjena <= 1.0:
    if ocjena < 0.6:
        ocjena_str = "F"
    elif ocjena >= 0.6 and ocjena < 0.7:
        ocjena_str = "D"
    elif ocjena >= 0.7 and ocjena < 0.8:
        ocjena_str = "C"
    elif ocjena >= 0.8 and ocjena < 0.9:
        ocjena_str = "B"
    else:
        ocjena_str = "A"
else:
    print("Broj nije u rasponu!")
    sys.exit()

print("Ocjena: " + ocjena_str)
