import re

file = open("mbox-short.txt")

lista = []

for line in file:
    if line.startswith("From"):
        mail = re.search(r'[\w\.-]+@[\w\.-]+', line)
        lista.append(mail.group(0))

dictionary = dict()
for x in lista:
    hostname = x.split('@')[1]
    if hostname not in dictionary:
        dictionary[hostname] = 1
    else:
        dictionary[hostname] +=1

print(dictionary)