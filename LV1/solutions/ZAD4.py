list_numbers = []

while(True):
    x = input("Unesite brojeve : ")
    if x == "Done":
        break
    try:
        list_numbers.append(float(x))
    except ValueError:
        print("Nije broj.")
        
print("List br: ", len(list_numbers))
print("List min: ", min(list_numbers))
print("List max: ", max(list_numbers))
print("List srednja: ", sum(list_numbers)/len(list_numbers))