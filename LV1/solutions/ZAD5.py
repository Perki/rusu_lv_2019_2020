import re

ime_dat = input ("Unesite ime tekstualne datoteke: ")
file = open(ime_dat)

vrijednosti = []

for line in file:
    if line.startswith("X-DSPAM-Confidence: "):
        broj = re.findall("\d+\.\d+", line)
        vrijednosti.append(float(broj[0]))

print("Srednja vrijednost pouzdanosti: ", sum(vrijednosti)/len(vrijednosti))